
#include <gudev/gudev.h>

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define INTEL_WMI_THUNDERBOLT_GUID "86CCFD48-205E-4A77-9C48-2021CBEDE341"


static void
handle_uevent (GUdevClient *client,
               gchar       *action,
               GUdevDevice *device,
               gpointer     user_data)
{
  const char *name;
  const char *path;

  name = g_udev_device_get_name (device);
  path = g_udev_device_get_sysfs_path (device);

  g_print ("uevent: %s for %s @ %s\n", action, name, path);

  if (g_str_equal (action, "add") &&
      g_str_has_prefix (name, "domain"))
    {
      const char *bootacl;

      bootacl = g_udev_device_get_sysfs_attr (device, "boot_acl");
      g_print (" boot_acl: %s\n", bootacl);
    }
}

static int
force_power (GUdevClient *udev, gboolean onoff)
{
  g_autoptr(GUdevEnumerator) e = NULL;
  g_autofree char *path = NULL;
  GList *lst, *l;
  ssize_t n;
  int fd;

  e = g_udev_enumerator_new (udev);

  g_udev_enumerator_add_match_subsystem (e, "wmi");
  g_udev_enumerator_add_match_property (e, "DRIVER", "intel-wmi-thunderbolt");

  lst = g_udev_enumerator_execute (e);
  l = lst;

  while (l != NULL)
    {
      g_autofree char *path_fp = NULL;
      g_autoptr(GUdevDevice) d = l->data;
      GList *next = l->next;

      if (path_fp == NULL)
	{
	  const char *syspath;

	  syspath = g_udev_device_get_sysfs_path (d);
	  path_fp = g_build_filename (syspath, "force_power", NULL);

	  g_debug ("checking %s", path_fp);
	  if (g_file_test (path_fp, G_FILE_TEST_IS_REGULAR))
	    path = g_steal_pointer (&path_fp);
	}

      lst = g_list_delete_link (lst, l);
      l = next;
    }

  if (path == NULL)
    {
      g_printerr ("could not find force_power device\n");
      return EXIT_FAILURE;
    }

  fd = open (path, O_WRONLY);

  if (fd < 0)
    {
      g_printerr ("could not open device\n");
      return EXIT_FAILURE;
    }
  n = write (fd, (onoff ? "1" : "0"), 1);

  if (n != 1)
    {
      g_printerr ("failed to force power: %s\n",
		  g_strerror (errno));
      return EXIT_FAILURE;
    }

  (void) close (fd);

  return EXIT_SUCCESS;
}

static GMainLoop *loop = NULL;

static void
handle_signal(int signo)
{
  g_print ("got signal: %d\n", signo);

  if (loop && signo == SIGINT)
    {
      g_main_quit (loop);
      g_clear_pointer (&loop, g_main_loop_unref);
    }
}

int
main(int argc, char **argv)
{
  g_autoptr(GUdevClient) udev;
  int r;

  udev = g_udev_client_new ((const char *[]) {"thunderbolt", NULL});

  if (udev == NULL)
    {
      g_printerr ("could not create udev client\n");
      return EXIT_FAILURE;
    }

  g_signal_connect (udev, "uevent", (GCallback) handle_uevent, NULL);

  r = force_power (udev, TRUE);

  if (r == EXIT_FAILURE)
    {
      g_printerr ("could not force power\n");
      return r;
    }

  signal (SIGINT, handle_signal);
  loop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run (loop);

  g_print("resetting force power\n");
  force_power (udev, FALSE);

  return EXIT_SUCCESS;
}
